import java.util.Random;

public class Die {
	private int pips;
	private Random random;
	
	public void initialValue() {
		this.pips = 1;
		this.random = new Random();
	}
	public int getPips() {
		return pips;
	}
	public void roll(int pips) {
		this.pips = random.nextInt(6)+1;
	}
	public String toString() {
		return "Value of pips is"+ this.pips;
	}
	

}
