import java.util.Arrays;

public class Board {

	private Die die1;
	private Die die2;
	public boolean[] closedTiles = new boolean[11];
	
	public void initialFrields() {
		die1 = new Die();
		die2 = new Die();
		closedTiles = new boolean[11];
		Arrays.fill(closedTiles, false);
			
		}
	public String toString() {
		String s = "";
		for (int i =0 ; i < closedTiles.length ;i++) {
			if(closedTiles[i]) {
				s = " "+"x";
			}
			else {
				s = " "+(i+1);
			}
		}
		return s;
	}
	public boolean playATurn() {
		die1.roll(die1.getPips());
		die2.roll(die2.getPips());
		System.out.println(die1.getPips());
		System.out.println(die2.getPips());
		int sumTwoPip = die1.getPips()+die2.getPips();
		boolean check = false;
		
		if(closedTiles[sumTwoPip]) {
			System.out.println((sumTwoPip)+"is already closed");
			check = true;
		}
		else {
			System.out.println("Closing tile: "+(sumTwoPip));
			closedTiles[sumTwoPip] = true;
			check = false;
		}
		return check;
		
	}
	
	
	

	
}
